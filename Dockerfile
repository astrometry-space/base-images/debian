FROM debian:10.4-slim

RUN apt-get update

RUN apt-get --yes install astrometry.net wget

RUN apt-get clean

RUN rm -rf /var/lib/apt-lists/*